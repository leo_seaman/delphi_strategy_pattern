unit OptionsUnit;

interface

uses
  AccountUnit;

type
  TOptions = class
  private
    FRoundingAccount: TAccount;
    FDistributeRoundingToAccount: boolean;
  public
    property DistributeRoundingToAccount : boolean read FDistributeRoundingToAccount write FDistributeRoundingToAccount;
    property RoundingAccount : TAccount read FRoundingAccount write FRoundingAccount;
  end;

var
  options : TOptions;

implementation

{ TOptions }

initialization
  options := TOptions.Create;

finalization
  options.Free;

end.
