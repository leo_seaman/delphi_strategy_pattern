unit frmMainUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  AccountManagerUnit, Vcl.Grids, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls;

type
  TForm1 = class(TForm)
    edtAmount: TEdit;
    btnDistribute: TButton;
    lvAccounts: TListView;
    Label1: TLabel;
    edtAccountName: TEdit;
    btnAddAccount: TButton;
    btnClearAccounts: TButton;
    rgRoundingErrorOption: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnDistributeClick(Sender: TObject);
    procedure btnAddAccountClick(Sender: TObject);
    procedure btnClearAccountsClick(Sender: TObject);
    procedure rgRoundingErrorOptionClick(Sender: TObject);
  private
    { Private declarations }
    FAccounts : TAccountManager;
    procedure DisplayAccounts;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  AccountUnit, OptionsUnit;

{$R *.dfm}


procedure TForm1.btnClearAccountsClick(Sender: TObject);
begin
  FAccounts.Accounts.Clear;
  options.RoundingAccount := nil;
  DisplayAccounts;
end;

procedure TForm1.btnDistributeClick(Sender: TObject);
begin
  // note that this code doesn't change
  FAccounts.DistributeEvenly( StrToFloatDef(edtAmount.Text, 0) );
  DisplayAccounts;
end;

procedure TForm1.btnAddAccountClick(Sender: TObject);
begin
  FAccounts.Accounts.Add( TAccount.create( edtAccountName.Text) );
  options.RoundingAccount := FAccounts.Accounts[0];
  DisplayAccounts;
end;

procedure TForm1.DisplayAccounts;
var
  account : TAccount;
  li : TListItem;
begin
  lvAccounts.Clear;
  for account in FAccounts.Accounts do
  begin
    li := lvAccounts.Items.Add;
    li.Caption := account.AccountName;
    li.SubItems.Add( format('%m', [account.Balance] ));
  end;
  li := lvAccounts.Items.Add;
  li.Caption := 'Balance';
  li.SubItems.Add( format('%m', [FAccounts.Balance] ));
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  FAccounts := TAccountManager.Create;
  options.DistributeRoundingToAccount := True;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  FAccounts.Free;
end;

procedure TForm1.rgRoundingErrorOptionClick(Sender: TObject);
begin
  case rgRoundingErrorOption.ItemIndex of
    0: options.DistributeRoundingToAccount := true;
    1: options.DistributeRoundingToAccount := false;
  end;
end;

end.
