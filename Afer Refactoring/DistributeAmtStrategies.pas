unit DistributeAmtStrategies;

interface

uses
  System.Generics.Collections,
  AccountUnit,
  OptionsUnit;

type
  // abstract strategy to distribute amounts into an account
  // strategies can override the virtual methods to change
  // behavior
  TDisributeAmtIntoAccount = class
  private
    FAmount: Currency;
    FAccounts: TAccountList;
  protected
    FRoundingError : currency;
    procedure DistributeAmount; virtual;
    procedure HandleRoundingError; virtual; abstract;
  public
    constructor Create(AAmount : Currency; AAcounts : TAccountList);
    property Amount : Currency read FAmount write FAmount;
    property Accounts : TAccountList read FAccounts write FAccounts;
    procedure Execute; virtual;
  end;

  // distribute evently and place rounding error in last account
  TDistributeEvenlyIntoAccount = class(TDisributeAmtIntoAccount)
  protected
    procedure HandleRoundingError; override;
  end;

  // distribute evenly, and place rounding error specified in RoundingAccount property
  TDistributeWithRoundingAccount = class(TDisributeAmtIntoAccount)
  private
    FRoundingAccount: TAccount;
  protected
    procedure HandleRoundingError; override;
  public
    constructor Create(AAmount : Currency; AAccounts : TAccountList; ARoundingAccount : TAccount);
    property RoundingAccount : TAccount read FRoundingAccount write FRoundingAccount;
  end;

  TDistributeAmtIntoAccountFactory = class
  public
    class function Create(AAmount : Currency; AAccounts : TAccountList) : TDisributeAmtIntoAccount;
  end;

implementation

uses Math;

{ TDistributeAmtIntoAccount }

constructor TDisributeAmtIntoAccount.Create(AAmount: Currency;
  AAcounts: TAccountList);
begin
  FAmount := AAmount;
  FAccounts := AAcounts;
end;

{ TDistributeEvenlyIntoAccount }


{ TDistributeAmtIntoAccountFactory }

class function TDistributeAmtIntoAccountFactory.Create(AAmount: Currency;
  AAccounts: TAccountList): TDisributeAmtIntoAccount;
begin
  if options.DistributeRoundingToAccount then
    result := TDistributeWithRoundingAccount.Create(AAmount, AAccounts, options.RoundingAccount)
  else
    result := TDistributeEvenlyIntoAccount.Create(AAmount, AAccounts);
end;

{ TDistributeWithRoundingAccount }

constructor TDistributeWithRoundingAccount.Create(AAmount: Currency;
  AAccounts: TAccountList; ARoundingAccount: TAccount);
begin
  inherited Create(AAmount, AAccounts);
  FRoundingAccount := ARoundingAccount;
end;


procedure TDisributeAmtIntoAccount.Execute;
begin
  DistributeAmount;
  HandleRoundingError;
end;

procedure TDisributeAmtIntoAccount.DistributeAmount;
var
  account : TAccount;
  distribute : currency;
begin
  // common code moved here from sub-class Execute() method
  distribute := RoundTo(Amount / Accounts.Count, -2);
  FRoundingError := Amount - (distribute * Accounts.Count);

  for Account in Accounts do begin
    Account.Add(distribute);
  end;
end;

procedure TDistributeWithRoundingAccount.HandleRoundingError;
begin
  // add the rounding error to the account specified as
  // a property of the class.
  FRoundingAccount.add(FRoundingError);
end;

{ TDistributeEvenlyIntoAccount }

procedure TDistributeEvenlyIntoAccount.HandleRoundingError;
var
  account : TAccount;
begin
  // add the rounding error to the last account
  account := Accounts[Accounts.count -1]; // get last account in list
  account.add(FRoundingError);
end;

end.
