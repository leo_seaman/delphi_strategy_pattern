unit AccountManagerUnit;

interface

uses
  System.Generics.Collections,
  AccountUnit;

type
  TAccountManager = class
  private
    FAccounts: TAccountList;
  public
    constructor Create;
    destructor Destroy; override;
    procedure DistributeEvenly(Amount : currency);
    function Balance : currency;
    property Accounts : TAccountList read FAccounts write FAccounts;
  end;

implementation

{ TFoo }

uses DistributeAmtStrategies;

function TAccountManager.Balance: currency;
var
  Account : TAccount;
begin
  result := 0;
  for Account in FAccounts do
    result := result + Account.Balance;
end;

constructor TAccountManager.Create;
begin
  FAccounts := TAccountList.Create(true);
end;

destructor TAccountManager.Destroy;
begin
  FAccounts.Free;
end;

procedure TAccountManager.DistributeEvenly(Amount: currency);
var
  DisributeAmtIntoAccount : TDisributeAmtIntoAccount;
begin
  // code that was here has been refactored to the
  // TDistributeAmtIntoAccount strategy classes
  DisributeAmtIntoAccount := TDistributeAmtIntoAccountFactory.Create(Amount, FAccounts);
  DisributeAmtIntoAccount.Execute;
  DisributeAmtIntoAccount.Free;
end;

end.
