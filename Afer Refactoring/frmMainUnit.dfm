object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 303
  ClientWidth = 494
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    494
    303)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 88
    Width = 69
    Height = 13
    Caption = 'Account Name'
  end
  object edtAmount: TEdit
    Left = 8
    Top = 141
    Width = 121
    Height = 21
    TabOrder = 0
    Text = '1000.00'
  end
  object btnDistribute: TButton
    Left = 135
    Top = 139
    Width = 84
    Height = 25
    Caption = 'Distribute'
    TabOrder = 1
    OnClick = btnDistributeClick
  end
  object lvAccounts: TListView
    Left = 8
    Top = 168
    Width = 368
    Height = 127
    Anchors = [akLeft, akTop, akRight, akBottom]
    Columns = <
      item
        Caption = 'Account'
        Width = 250
      end
      item
        Alignment = taRightJustify
        Caption = 'Balance'
        Width = 100
      end>
    ReadOnly = True
    RowSelect = True
    TabOrder = 2
    ViewStyle = vsReport
  end
  object edtAccountName: TEdit
    Left = 8
    Top = 107
    Width = 121
    Height = 21
    TabOrder = 3
  end
  object btnAddAccount: TButton
    Left = 135
    Top = 105
    Width = 84
    Height = 25
    Caption = 'Add Account'
    TabOrder = 4
    OnClick = btnAddAccountClick
  end
  object btnClearAccounts: TButton
    Left = 382
    Top = 270
    Width = 104
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Clear Accounts'
    TabOrder = 5
    OnClick = btnClearAccountsClick
  end
  object rgRoundingErrorOption: TRadioGroup
    Left = 8
    Top = 6
    Width = 369
    Height = 76
    Caption = 'Place rounding error'
    ItemIndex = 0
    Items.Strings = (
      'Placed in first account'
      'Placed in last account')
    TabOrder = 6
    OnClick = rgRoundingErrorOptionClick
  end
end
