unit frmMainUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  AccountManagerUnit, Vcl.Grids, Vcl.StdCtrls, Vcl.ComCtrls;

type
  TForm1 = class(TForm)
    edtAmount: TEdit;
    btnDistribute: TButton;
    lvAccounts: TListView;
    Label1: TLabel;
    edtAccountName: TEdit;
    Button1: TButton;
    btnClearAccounts: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnDistributeClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure btnClearAccountsClick(Sender: TObject);
  private
    { Private declarations }
    FAccounts : TAccountManager;
    procedure DisplayAccounts;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  AccountUnit;

{$R *.dfm}


procedure TForm1.btnClearAccountsClick(Sender: TObject);
begin
  FAccounts.Accounts.Clear;
  DisplayAccounts;
end;

procedure TForm1.btnDistributeClick(Sender: TObject);
begin
  FAccounts.DistributeEvenly( StrToFloatDef(edtAmount.Text, 0) );
  DisplayAccounts;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  FAccounts.Accounts.Add( TAccount.create( edtAccountName.Text) );
  DisplayAccounts;
end;

procedure TForm1.DisplayAccounts;
var
  account : TAccount;
  li : TListItem;
begin
  lvAccounts.Clear;
  for account in FAccounts.Accounts do
  begin
    li := lvAccounts.Items.Add;
    li.Caption := account.AccountName;
    li.SubItems.Add( format('%m', [account.Balance] ));
  end;
  li := lvAccounts.Items.Add;
  li.Caption := 'Balance';
  li.SubItems.Add( format('%m', [FAccounts.Balance] ));
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  FAccounts := TAccountManager.Create;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  FAccounts.Free;
end;

end.
