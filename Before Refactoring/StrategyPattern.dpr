program StrategyPattern;

uses
  Vcl.Forms,
  frmMainUnit in 'frmMainUnit.pas' {Form1},
  AccountUnit in 'AccountUnit.pas',
  AccountManagerUnit in 'AccountManagerUnit.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
