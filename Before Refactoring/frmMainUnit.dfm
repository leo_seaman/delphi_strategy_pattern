object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 218
  ClientWidth = 527
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    527
    218)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 69
    Height = 13
    Caption = 'Account Name'
  end
  object edtAmount: TEdit
    Left = 8
    Top = 61
    Width = 121
    Height = 21
    TabOrder = 0
    Text = '1000.00'
  end
  object btnDistribute: TButton
    Left = 135
    Top = 59
    Width = 84
    Height = 25
    Caption = 'Distribute'
    TabOrder = 1
    OnClick = btnDistributeClick
  end
  object lvAccounts: TListView
    Left = 8
    Top = 90
    Width = 401
    Height = 118
    Anchors = [akLeft, akTop, akRight, akBottom]
    Columns = <
      item
        Caption = 'Account'
        Width = 250
      end
      item
        Alignment = taRightJustify
        Caption = 'Balance'
        Width = 100
      end>
    ReadOnly = True
    RowSelect = True
    TabOrder = 2
    ViewStyle = vsReport
  end
  object edtAccountName: TEdit
    Left = 8
    Top = 27
    Width = 121
    Height = 21
    TabOrder = 3
  end
  object Button1: TButton
    Left = 135
    Top = 25
    Width = 84
    Height = 25
    Caption = 'Add Account'
    TabOrder = 4
    OnClick = Button1Click
  end
  object btnClearAccounts: TButton
    Left = 415
    Top = 185
    Width = 104
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Clear Accounts'
    TabOrder = 5
    OnClick = btnClearAccountsClick
  end
end
