unit AccountManagerUnit;

interface

uses
  System.Generics.Collections,
  AccountUnit;

type
  TAccountManager = class
  private
    FAccounts: TAccountList;
  public
    constructor Create;
    destructor Destroy;
    procedure DistributeEvenly(Amount : currency);
    function Balance : currency;
    property Accounts : TAccountList read FAccounts write FAccounts;
  end;

implementation

uses Math;

{ TFoo }

function TAccountManager.Balance: currency;
var
  Account : TAccount;
begin
  result := 0;
  for Account in FAccounts do
    result := result + Account.Balance;
end;

constructor TAccountManager.Create;
begin
  FAccounts := TAccountList.Create(true);
end;

destructor TAccountManager.Destroy;
begin
  FAccounts.Free;
end;

// distribute amount evenly into FAccounts
procedure TAccountManager.DistributeEvenly(Amount: currency);
var
  roundingError : currency;
  distributeAmt : currency;
  account : TAccount;
begin
  distributeAmt := RoundTo( Amount / FAccounts.Count, -2);
  roundingError := Amount - (distributeAmt * FAccounts.Count);

  for account in FAccounts do
    account.Add(distributeAmt);

  Accounts[Accounts.Count -1].Add(roundingError);
end;

end.
